({
    generateChart : function(component, event, helper) {
    var chartdata = {
        labels: ['Sunday','Monday', 'Tuesday', 'Wednesday', 'Thursday', 'Friday', 'Saturday'],
        datasets: [
            {
                label:'Day',
                data: [110, 290, 150, 250, 500, 420, 100],
                backgroundColor: [
                    "#52BE80",
                    "#76D7C4",
                    "#1E8449",
                    "#2ECC71",
                    "#FFB74D",
                    "#E67E22",
                    "#F8C471"
                ],
                borderColor:'rgba(62, 159, 222, 1)',
                fill: false,
                pointBackgroundColor: "Red",
                pointBorderWidth: 2,
                pointHoverRadius: 5,
                pointRadius: 3,
                bezierCurve: true,
                pointHitRadius: 10
            }
        ]
    }
    //Get the context of the canvas element we want to select
    var ctx = component.find("linechart").getElement();
    var lineChart = new Chart(ctx ,{
        type: 'bar',
        data: chartdata,
        options: {	
            legend: {
                position: 'bottom',
                padding: 10,
            },
            responsive: true
        }
    });
}
})