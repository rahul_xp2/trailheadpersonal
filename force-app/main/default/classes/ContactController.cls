public with sharing class ContactController {
    @AuraEnabled(cacheable=true)
    public static List<Contact> getContactList(){
        return [select id, firstname, lastname,email from contact];  
    }
}
