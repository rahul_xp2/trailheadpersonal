public with sharing class SurveyController {
   @AuraEnabled(cacheable=true)
   public static List<Question__C> showQuestions(String surveyRecId){
       return [select id, name,survey__C from question__c where survey__C=: surveyRecId ];
   }
}
