import { LightningElement} from "lwc";

export default class HelloWorld extends LightningElement {
  result='';
  get showOutput(){
    return 'Welcome '+this.result;
  }
  handleChange(e){
    this.result=e.target.value;
  }
}
