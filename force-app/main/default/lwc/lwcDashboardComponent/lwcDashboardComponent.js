import { LightningElement, wire, track} from 'lwc';
import { loadScript } from 'lightning/platformResourceLoader';
import chartJSResource from '@salesforce/resourceUrl/ChartJs';
import showDashboard from '@salesforce/apex/AccountController.showDashboard';


export default class LwcDashboardComponent extends LightningElement {
    @track listOFAccounts;
    lineChart='';
    chartdata;
    @track mapofResults;
    @track errorMsg;
   @track noOfCount=[];
   @track nameOfDays=[];
    xData;
    yData;
    callApexMethod(){
        console.log('CallPe');
        showDashboard().then(result=>{
            // eslint-disable-next-line guard-for-in
            for(let abc in result){
                this.noOfCount.push(result[abc]);
                this.nameOfDays.push(abc);
            }
           
        // eslint-disable-next-line no-unused-vars
        }).catch(error=>{
            // eslint-disable-next-line no-console
            console.log('Test Error');
        })
    }

    connectedCallback(){
        // eslint-disable-next-line no-console
     // eslint-disable-next-line no-debugger
     
        if(this.nameOfDays.length===0){
            // eslint-disable-next-line no-console
            console.log('Inside caller');
            this.callApexMethod();
            console.log(JSON.stringify(this.noOfCount)+'=========='+this.nameOfDays);
            console.log('After..............');
        }
            this.chartdata = {
                labels:  this.nameOfDays,
                datasets: [
                    {
                        label:'Day_Name',
                        data:  this.noOfCounts,
                        backgroundColor: [
                            "blue",
                            "blue"
                        ]
                    }
                ]
            }
            console.log('Data loaded');
            this.renderedCallback1();
        
    }
    

  

    renderedCallback1() { // invoke the method when component rendered or loaded
        // eslint-disable-next-line no-console
        console.log('test connectd');
        Promise.all([
            loadScript(this, chartJSResource)
        ])
        .then(() => { 
            let ctx=this.template.querySelector('.canvasClass');
                        this.lineChart =new window.Chart(ctx ,{
                            type: 'bar',
                            data: this.chartdata,
                            options: {	
                                legend: {
                                    position: 'bottom',
                                    padding: 2,
                                },
                                responsive: true
                            }
                        });
                        // eslint-disable-next-line no-console
                        console.log('This is default behaviour..');
                  
     
          
            
        })
        .catch(error => {
            this.errorMsg = error;
           
        });
       
    }
   
}