import { LightningElement, wire, track,api } from 'lwc';
import showQuestions from '@salesforce/apex/SurveyController.showQuestions';

export default class SurveyShow extends LightningElement {
   @api recordId;
   @track errorMsg;
   @track showQuestionRecords;
    @wire(showQuestions,{surveyRecId:'$recordId'})
    showContacts({data,error}){
        if(data){
            this.showQuestionRecords=data;
        }
        else if(error){
            this.errorMsg=error;
        }
    }
    handleSave(){
      //  let str=this.template.querySelector();
    }
    handleChange(){
       // eslint-disable-next-line no-console
       console.log('======'+JSON.stringify(this.showQuestionRecords)); 
    }
}