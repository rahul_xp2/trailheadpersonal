#!/bin/bash

# Test Data load script for Foundation data into the default org
# Default org will be your scratch org or whatever is set with "sfdx force:config:set defaultusername=OrgAlias"
# Because looking inside a Static Resource is considered a callout, we cannot run one Anonymous script to load all the data, as
# we would need to do callouts after a DML.
# This runs a bunch of Anonymous scripts in a row to insert all the data we need.

# Note that the following will need to be run before these scripts will work:
sfdx force:source:deploy -p "TestData/"

# Init line for loading the data. Put in namespaces here - they CAN BE NULL (and should be in dev environments!)
# TestData td = new TestData(StaticRecourceName, FoundationNamespace, RBMNamespace, CRA_CNamespace, CTMSNamespace);
INITLINE="TestData td = new TestData('FoundationTestData');"

# List of files in this Static Resource to insert, in their specific order
declare -a DATAFILES=(
    "'Account',1"
    "'Account',2"
    "'Account_Relationship__c',1"
    "'Contact',1"
    "'Contact',2"
    "'Investigational_Product__c',1" 
    "'Study__c',1" 
    "'Study_Team_Member__c',1" 
    "'Study_Site__c',1" 
    "'Subject__c',1" 
    "'Informed_Consent__c',1" 
    "'Informed_Consent_Version__c',1" 
    "'Subject_Informed_Consent__c',1" 
    "'Site_Visit__c',1" 
    "'Site_Visit_Participant__c',1" 
    "'Site_Visit_Report_Template__c',1" 
    "'Site_Visit_Report__c',1" 
    "'Subject_Visit__c',1" 
    "'Protocol_Deviation__c',1" 
    "'Query__c',1" 
    "'Adverse_Event__c',1" 
    "'Action_Item__c',1" 
    "'Questionnaire__c',1" 
    "'Question_Group__c',1" 
    "'Question__c',1" 
    "'Capabilities_and_Equipment__c',1"
    "'Capabilities_and_Equipment__c',2"
)

for THISFILE in "${DATAFILES[@]}"
do
    echo "$INITLINE td.insertTestData($THISFILE);" | sfdx force:apex:execute
    if [ $? -ne 0 ]; then
        # Exit out if there is an error. Unfortunately, sfdx force:apex:execute does not yet return an error code if execution fails.
        # It is on the SFDX roadmap: https://github.com/forcedotcom/cli/issues/77
        exit 1
    fi
done
